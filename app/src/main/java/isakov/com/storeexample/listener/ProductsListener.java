package isakov.com.storeexample.listener;

import isakov.com.storeexample.models.ProductModel;

/**
 * Created by Isakov on 05-Oct-17.
 */

public interface ProductsListener {
    void addToBasket(ProductModel model);
}
