package isakov.com.storeexample.listener;

import java.io.Serializable;

/**
 * Created by Isakov on 05-Oct-17.
 */

public interface DialogListener extends Serializable{
    void addItem(String name, int price);
}
