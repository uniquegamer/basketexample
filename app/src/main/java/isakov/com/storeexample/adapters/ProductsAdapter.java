package isakov.com.storeexample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isakov.com.storeexample.R;
import isakov.com.storeexample.models.ProductModel;

/**
 * Created by Isakov on 05-Oct-17.
 */

public class ProductsAdapter extends BaseAdapter {

    private ArrayList<ProductModel> list;
    private Context context;

    public ProductsAdapter(Context context, ArrayList<ProductModel> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.basket_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        ProductModel model = list.get(i);

        holder.tvId.setText(String.valueOf(model.getId()));
        holder.tvName.setText(model.getName());
        holder.tvPrice.setText(String.valueOf(model.getPrice()));

        return view;
    }

    private class ViewHolder {
        private TextView tvId, tvName, tvPrice;

        ViewHolder(View view) {
            tvId = view.findViewById(R.id.tvId);
            tvName = view.findViewById(R.id.tvName);
            tvPrice = view.findViewById(R.id.tvPrice);
        }
    }
}
