package isakov.com.storeexample;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import isakov.com.storeexample.listener.ProductsListener;
import isakov.com.storeexample.models.ProductModel;
import isakov.com.storeexample.ui.DialogItemAddition;
import isakov.com.storeexample.ui.ProductsFragment;
import isakov.com.storeexample.ui.BasketFragment;

public class MainActivity extends AppCompatActivity implements ProductsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();

        switchFragments();
    }

    private void switchFragments() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.basketContainer, new BasketFragment())
                .replace(R.id.productsContainer, new ProductsFragment())
                .commit();

    }

    private void initializeUI() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle(getString(R.string.basket));
    }

    @Override
    public void addToBasket(ProductModel model) {
        BasketFragment fragment = (BasketFragment) getSupportFragmentManager().findFragmentById(R.id.basketContainer);
        if (fragment != null) fragment.getProduct(model);
    }
}
