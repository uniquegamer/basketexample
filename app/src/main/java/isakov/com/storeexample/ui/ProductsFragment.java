package isakov.com.storeexample.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import isakov.com.storeexample.R;
import isakov.com.storeexample.adapters.ProductsAdapter;
import isakov.com.storeexample.listener.DialogListener;
import isakov.com.storeexample.listener.ProductsListener;
import isakov.com.storeexample.models.ProductModel;

/**
 * Created by Isakov on 05-Oct-17.
 */

public class ProductsFragment extends Fragment implements DialogListener, AdapterView.OnItemClickListener {

    private ListView listView;
    private ProductsAdapter adapter;
    private ProductsListener mCallback;

    private ArrayList<ProductModel> list = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        listView = view.findViewById(R.id.listView);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createItems();
    }

    private void createItems() {
        ProductModel model = new ProductModel();
        model.setId(1);
        model.setName("Product #1");
        model.setPrice(1000);
        list.add(model);

        adapter = new ProductsAdapter(getContext(), list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void addItem(String name, int price) {
        ProductModel model = new ProductModel();
        model.setName(name);
        model.setPrice(price);
        model.setId(list.size() + 1);
        list.add(model);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (ProductsListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
            + "must implement ProductListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu.findItem(0) == null) inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_item:
                DialogItemAddition dialog = DialogItemAddition.newInstance(this);
                dialog.show(getActivity().getSupportFragmentManager(), "dialog");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        mCallback.addToBasket((ProductModel) adapterView.getItemAtPosition(i));
    }
}
