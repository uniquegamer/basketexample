package isakov.com.storeexample.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import isakov.com.storeexample.R;
import isakov.com.storeexample.listener.DialogListener;

/**
 * Created by Isakov on 05-Oct-17.
 */

public class DialogItemAddition extends DialogFragment implements View.OnClickListener {

    private EditText etName, etPrice;
    private DialogListener mCallback;

    public static DialogItemAddition newInstance(DialogListener listener) {
        DialogItemAddition fragmentDialog = new DialogItemAddition();

        Bundle args = new Bundle();
        args.putSerializable("listener", listener);
        fragmentDialog.setArguments(args);

        return fragmentDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_addition, container, false);
        initializeUI(view);
        return view;
    }

    private void initializeUI(View view) {
        etName = view.findViewById(R.id.etName);
        etPrice = view.findViewById(R.id.etPrice);
        view.findViewById(R.id.btnCancel).setOnClickListener(this);
        view.findViewById(R.id.btnAdd).setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCallback = (DialogListener) getArguments().getSerializable("listener");
        initializeParams();
    }

    private void initializeParams() {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setTitle("Add new product");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnAdd:
                if (!etName.getText().toString().isEmpty() && !etPrice.getText().toString().isEmpty()) {
                    mCallback.addItem(etName.getText().toString(), Integer.parseInt(etPrice.getText().toString()));
                    dismiss();
                } else {
                    Toast.makeText(getContext(), getString(R.string.incorrect_data), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
