package isakov.com.storeexample.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import isakov.com.storeexample.R;
import isakov.com.storeexample.adapters.ProductsAdapter;
import isakov.com.storeexample.models.ProductModel;

/**
 * Created by Isakov on 05-Oct-17.
 */

public class BasketFragment extends Fragment implements View.OnClickListener {

    private ListView listView;
    private TextView tvTotal;
    private ArrayList<ProductModel> list = new ArrayList<>();
    private ProductsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basket, container, false);
        initializeUI(view);
        return view;
    }

    private void initializeUI(View view) {
        listView = view.findViewById(R.id.listView);
        tvTotal = view.findViewById(R.id.tvTotal);
        view.findViewById(R.id.btnClearAll).setOnClickListener(this);
        view.findViewById(R.id.btnCheckOut).setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ProductsAdapter(getContext(), list);
        listView.setAdapter(adapter);
    }

    public void getProduct(ProductModel model) {
        list.add(model);
        String total = String.format(getString(R.string.total), countSum(list), String.valueOf(list.size()));
        tvTotal.setText(total);
        adapter.notifyDataSetChanged();
    }

    private String countSum(ArrayList<ProductModel> list) {
        int sum = 0;
        for (ProductModel model : list) {
            sum += model.getPrice();
        }
        return String.valueOf(sum);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClearAll:
                list.clear();
                tvTotal.setText("");
                adapter.notifyDataSetChanged();
                break;
            case R.id.btnCheckOut:
                if (!tvTotal.getText().toString().isEmpty()) {
                    startActivity(new Intent(getContext(), OrderActivity.class)
                            .putExtra("list", list));
                } else {
                    Toast.makeText(getContext(), getString(R.string.empty_basket), Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
}
