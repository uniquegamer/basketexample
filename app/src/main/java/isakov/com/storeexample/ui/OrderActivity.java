package isakov.com.storeexample.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import isakov.com.storeexample.R;
import isakov.com.storeexample.adapters.ProductsAdapter;
import isakov.com.storeexample.models.ProductModel;

/**
 * Created by Isakov on 06-Oct-17.
 */

public class OrderActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView listView;
    private EditText etRecipient, etNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        initializeUI();
        setData();
    }

    private void initializeUI() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.listView);
        etRecipient = findViewById(R.id.etRecipient);
        etNumber = findViewById(R.id.etNumber);
        findViewById(R.id.btnSend).setOnClickListener(this);
    }

    private void setData() {
        listView.setAdapter(new ProductsAdapter(this, (ArrayList<ProductModel>) getIntent().getSerializableExtra("list")));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (!etNumber.getText().toString().isEmpty() && !etRecipient.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.order_sent), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.incorrect_data), Toast.LENGTH_SHORT).show();
        }
    }
}
